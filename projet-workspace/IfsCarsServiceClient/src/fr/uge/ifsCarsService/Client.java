/**
 * Client.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package fr.uge.ifsCarsService;

public class Client  implements java.io.Serializable {
    private java.lang.String adresseMail;

    private java.lang.String nomClient;

    private java.lang.String prenomClient;

    public Client() {
    }

    public Client(
           java.lang.String adresseMail,
           java.lang.String nomClient,
           java.lang.String prenomClient) {
           this.adresseMail = adresseMail;
           this.nomClient = nomClient;
           this.prenomClient = prenomClient;
    }


    /**
     * Gets the adresseMail value for this Client.
     * 
     * @return adresseMail
     */
    public java.lang.String getAdresseMail() {
        return adresseMail;
    }


    /**
     * Sets the adresseMail value for this Client.
     * 
     * @param adresseMail
     */
    public void setAdresseMail(java.lang.String adresseMail) {
        this.adresseMail = adresseMail;
    }


    /**
     * Gets the nomClient value for this Client.
     * 
     * @return nomClient
     */
    public java.lang.String getNomClient() {
        return nomClient;
    }


    /**
     * Sets the nomClient value for this Client.
     * 
     * @param nomClient
     */
    public void setNomClient(java.lang.String nomClient) {
        this.nomClient = nomClient;
    }


    /**
     * Gets the prenomClient value for this Client.
     * 
     * @return prenomClient
     */
    public java.lang.String getPrenomClient() {
        return prenomClient;
    }


    /**
     * Sets the prenomClient value for this Client.
     * 
     * @param prenomClient
     */
    public void setPrenomClient(java.lang.String prenomClient) {
        this.prenomClient = prenomClient;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Client)) return false;
        Client other = (Client) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.adresseMail==null && other.getAdresseMail()==null) || 
             (this.adresseMail!=null &&
              this.adresseMail.equals(other.getAdresseMail()))) &&
            ((this.nomClient==null && other.getNomClient()==null) || 
             (this.nomClient!=null &&
              this.nomClient.equals(other.getNomClient()))) &&
            ((this.prenomClient==null && other.getPrenomClient()==null) || 
             (this.prenomClient!=null &&
              this.prenomClient.equals(other.getPrenomClient())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAdresseMail() != null) {
            _hashCode += getAdresseMail().hashCode();
        }
        if (getNomClient() != null) {
            _hashCode += getNomClient().hashCode();
        }
        if (getPrenomClient() != null) {
            _hashCode += getPrenomClient().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Client.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ifsCarsService.uge.fr", "Client"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("adresseMail");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ifsCarsService.uge.fr", "adresseMail"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nomClient");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ifsCarsService.uge.fr", "nomClient"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("prenomClient");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ifsCarsService.uge.fr", "prenomClient"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
