/**
 * Vehicule.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package fr.uge.ifsCarsService;

public class Vehicule  implements java.io.Serializable {
    private boolean dispo;

    private java.lang.String idEmploye;

    private boolean louerAuMoinsUneFois;

    private java.lang.String matricule;

    private float prix;

    public Vehicule() {
    }

    public Vehicule(
           boolean dispo,
           java.lang.String idEmploye,
           boolean louerAuMoinsUneFois,
           java.lang.String matricule,
           float prix) {
           this.dispo = dispo;
           this.idEmploye = idEmploye;
           this.louerAuMoinsUneFois = louerAuMoinsUneFois;
           this.matricule = matricule;
           this.prix = prix;
    }


    /**
     * Gets the dispo value for this Vehicule.
     * 
     * @return dispo
     */
    public boolean isDispo() {
        return dispo;
    }


    /**
     * Sets the dispo value for this Vehicule.
     * 
     * @param dispo
     */
    public void setDispo(boolean dispo) {
        this.dispo = dispo;
    }


    /**
     * Gets the idEmploye value for this Vehicule.
     * 
     * @return idEmploye
     */
    public java.lang.String getIdEmploye() {
        return idEmploye;
    }


    /**
     * Sets the idEmploye value for this Vehicule.
     * 
     * @param idEmploye
     */
    public void setIdEmploye(java.lang.String idEmploye) {
        this.idEmploye = idEmploye;
    }


    /**
     * Gets the louerAuMoinsUneFois value for this Vehicule.
     * 
     * @return louerAuMoinsUneFois
     */
    public boolean isLouerAuMoinsUneFois() {
        return louerAuMoinsUneFois;
    }


    /**
     * Sets the louerAuMoinsUneFois value for this Vehicule.
     * 
     * @param louerAuMoinsUneFois
     */
    public void setLouerAuMoinsUneFois(boolean louerAuMoinsUneFois) {
        this.louerAuMoinsUneFois = louerAuMoinsUneFois;
    }


    /**
     * Gets the matricule value for this Vehicule.
     * 
     * @return matricule
     */
    public java.lang.String getMatricule() {
        return matricule;
    }


    /**
     * Sets the matricule value for this Vehicule.
     * 
     * @param matricule
     */
    public void setMatricule(java.lang.String matricule) {
        this.matricule = matricule;
    }


    /**
     * Gets the prix value for this Vehicule.
     * 
     * @return prix
     */
    public float getPrix() {
        return prix;
    }


    /**
     * Sets the prix value for this Vehicule.
     * 
     * @param prix
     */
    public void setPrix(float prix) {
        this.prix = prix;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Vehicule)) return false;
        Vehicule other = (Vehicule) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.dispo == other.isDispo() &&
            ((this.idEmploye==null && other.getIdEmploye()==null) || 
             (this.idEmploye!=null &&
              this.idEmploye.equals(other.getIdEmploye()))) &&
            this.louerAuMoinsUneFois == other.isLouerAuMoinsUneFois() &&
            ((this.matricule==null && other.getMatricule()==null) || 
             (this.matricule!=null &&
              this.matricule.equals(other.getMatricule()))) &&
            this.prix == other.getPrix();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += (isDispo() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        if (getIdEmploye() != null) {
            _hashCode += getIdEmploye().hashCode();
        }
        _hashCode += (isLouerAuMoinsUneFois() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        if (getMatricule() != null) {
            _hashCode += getMatricule().hashCode();
        }
        _hashCode += new Float(getPrix()).hashCode();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Vehicule.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ifsCarsService.uge.fr", "Vehicule"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dispo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ifsCarsService.uge.fr", "dispo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idEmploye");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ifsCarsService.uge.fr", "idEmploye"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("louerAuMoinsUneFois");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ifsCarsService.uge.fr", "louerAuMoinsUneFois"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("matricule");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ifsCarsService.uge.fr", "matricule"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("prix");
        elemField.setXmlName(new javax.xml.namespace.QName("http://ifsCarsService.uge.fr", "prix"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "float"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
