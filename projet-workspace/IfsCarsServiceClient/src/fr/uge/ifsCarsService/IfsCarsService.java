/**
 * IfsCarsService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package fr.uge.ifsCarsService;

public interface IfsCarsService extends java.rmi.Remote {
    public void afficherVehiculesPourLouer() throws java.rmi.RemoteException;
    public void rendreVehicule(java.lang.String matricule, java.lang.String notesClient) throws java.rmi.RemoteException;
    public fr.uge.ifsCarsService.Vehicule getVehicule(java.lang.String matricule) throws java.rmi.RemoteException;
    public void deleteVehicule(boolean isDispo, java.lang.String matricule) throws java.rmi.RemoteException;
    public void getEmploye(java.lang.String idEmploye) throws java.rmi.RemoteException;
    public void acheterVehiculeDevise(java.lang.String adresseMail, java.lang.String devise) throws java.rmi.RemoteException;
    public void afficherVehiculeDevise(java.lang.String devise) throws java.rmi.RemoteException;
    public void louerVehicule(java.lang.String matricule, java.lang.String idEmploye) throws java.rmi.RemoteException;
    public fr.uge.ifsCarsService.Client getClient(java.lang.String adresseMail) throws java.rmi.RemoteException;
    public void afficherVehicule() throws java.rmi.RemoteException;
    public void acheterVehicule(java.lang.String adresseMail) throws java.rmi.RemoteException;
    public boolean ajouterAuPanier(java.lang.String matricule, java.lang.String adresseMail) throws java.rmi.RemoteException;
    public java.lang.String[] retournerPanier(java.lang.String adresseMail) throws java.rmi.RemoteException;
    public void supprimerPanierClientLigne(java.lang.String matricule, java.lang.String adresseMail) throws java.rmi.RemoteException;
    public double convertCurrency(java.lang.String devise, double prix) throws java.rmi.RemoteException;
    public void inscrireClient(java.lang.String adresseMail, java.lang.String nomClient, java.lang.String prenomClient) throws java.rmi.RemoteException;
}
