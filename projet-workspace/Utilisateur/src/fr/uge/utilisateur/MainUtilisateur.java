package fr.uge.utilisateur;

import java.io.InvalidClassException;
import java.rmi.RemoteException;
import java.util.Scanner;

import javax.xml.rpc.ServiceException;

import fr.uge.ifsCarsService.IfsCarsService;
import fr.uge.ifsCarsService.IfsCarsServiceServiceLocator;

public class MainUtilisateur {

	public static void main(String[] args) throws ServiceException, RemoteException {
		//appel au SW
		IfsCarsService service = new IfsCarsServiceServiceLocator().getIfsCarsService();


		Scanner scan = new java.util.Scanner(System.in);
		System.out.println("tapez 1 si vous etes client, tapez 2 si vous etes employe");
		int user;
		String isClient;
		String email;
		String nom;
		String prenom;
		String matricule;
		do {
			user =scan.nextInt(); // 1 ou  2 
			if(user==1){
				System.out.println("Saisissez votre mail");
				email = scan.next();	
				if(service.getClient(email) == null) {
					System.out.println("Votre mail n existe pas");
					System.out.println("On va vous inscrire avec ce mail.");
					System.out.println("Saississez votre nom ");
					nom = scan.next();
					System.out.println("Veuillez saisir votre prénom");
					prenom = scan.next();
					service.inscrireClient(email, nom, prenom);
				}

				service.afficherVehicule();
				int choix;
				while(true) {
					do {

						System.out.println("Tapez 1. pour afficher les véhicules à disposition");
						System.out.println("Tapez 2. pour afficher les véhicules dans une autre devise");
						System.out.println("Tapez 3. pour ajouter un véhicule au panier");
						System.out.println("Tapez 4. pour afficher les véhicules du panier");
						System.out.println("Tapez 5. pour acheter les véhicules du panier");
						System.out.println("Tapez 6. pour acheter les véhicules du panier dans une autre devise");
						System.out.println("Tapez 7 pour quitter");
						choix = scan.nextInt();

					}while(choix>8 || choix <0);

					switch (choix) {
					case 1:
						service.afficherVehicule();
						break;
					case 2:
						System.out.println("2. Afficher les véhicules dans une autre devise");
						System.out.println("Saisir le code de la devise");
						String devise = scan.next();
						service.afficherVehiculeDevise(devise);
						break;
					case 3:
						do {
							System.out.println("Saisissez la maricule que vous voulez ajouter au panier");
							matricule = scan.next();
							service.ajouterAuPanier(matricule, email);

						}while(service.getVehicule(matricule) != null);
						break;
					case 4:
						System.out.println("Saisissez la maricule que vous voulez ajouter au panier");
						service.retournerPanier(email);
						break;
					case 5:
						System.out.println("5. Achat des véhicules du panier");
						service.acheterVehicule(email);
						break;
					case 6:
						System.out.println("6. Achat des véhicules du panier dans une autre devise");
						devise = scan.next();
						service.acheterVehiculeDevise(email, devise);;
						break;
					case 7:
						return;
					}
				}

				///FIN CLIENT 

			}
			if(user==2) { // employé
				String id;
				//do {
				System.out.println("Veuillez saisir votre identifiant");
				id = scan.next();

				//}while(/*service.getEmploye(id)*/ id !=2);

				//traitement 
				service.afficherVehiculesPourLouer();
				int choix;
				while(true) {
					do {

						System.out.println("Tapez 1. pour afficher les véhicules à disposition");
						System.out.println("Tapez 2. pour louer un véhicule");
						System.out.println("Tapez 3 pour rendre un véhicule");
						System.out.println("Tapez 4 pour quitter");
						choix = scan.nextInt();

					}while(choix>5 || choix <0);

					switch (choix) {
					case 1:
						service.afficherVehiculesPourLouer();
						break;
					case 2:
						System.out.println("Saisir l'immatriculation du véhicule que vous voulez louer ?");
						matricule = scan.next();
						service.louerVehicule(matricule, id);
						break;
					case 3:
						System.out.println("Saisir l'immatriculation du véhicule que vous voulez louer ?");
						matricule = scan.next();
						System.out.println("Notez cette location :) ");
						String notesClient = scan.next();
						service.rendreVehicule(matricule, notesClient);
						break;
					case 4:
						return;
					}
				}
			}

		} while(user!=1 || user !=2);

	}

}






