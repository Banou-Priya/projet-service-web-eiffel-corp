package fr.uge.ifsCarsService;

public class IfsCarsServiceProxy implements fr.uge.ifsCarsService.IfsCarsService {
  private String _endpoint = null;
  private fr.uge.ifsCarsService.IfsCarsService ifsCarsService = null;
  
  public IfsCarsServiceProxy() {
    _initIfsCarsServiceProxy();
  }
  
  public IfsCarsServiceProxy(String endpoint) {
    _endpoint = endpoint;
    _initIfsCarsServiceProxy();
  }
  
  private void _initIfsCarsServiceProxy() {
    try {
      ifsCarsService = (new fr.uge.ifsCarsService.IfsCarsServiceServiceLocator()).getIfsCarsService();
      if (ifsCarsService != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)ifsCarsService)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)ifsCarsService)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (ifsCarsService != null)
      ((javax.xml.rpc.Stub)ifsCarsService)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public fr.uge.ifsCarsService.IfsCarsService getIfsCarsService() {
    if (ifsCarsService == null)
      _initIfsCarsServiceProxy();
    return ifsCarsService;
  }
  
  public void afficherVehiculesPourLouer() throws java.rmi.RemoteException{
    if (ifsCarsService == null)
      _initIfsCarsServiceProxy();
    ifsCarsService.afficherVehiculesPourLouer();
  }
  
  public void rendreVehicule(java.lang.String matricule, java.lang.String notesClient) throws java.rmi.RemoteException{
    if (ifsCarsService == null)
      _initIfsCarsServiceProxy();
    ifsCarsService.rendreVehicule(matricule, notesClient);
  }
  
  public fr.uge.ifsCarsService.Vehicule getVehicule(java.lang.String matricule) throws java.rmi.RemoteException{
    if (ifsCarsService == null)
      _initIfsCarsServiceProxy();
    return ifsCarsService.getVehicule(matricule);
  }
  
  public void deleteVehicule(boolean isDispo, java.lang.String matricule) throws java.rmi.RemoteException{
    if (ifsCarsService == null)
      _initIfsCarsServiceProxy();
    ifsCarsService.deleteVehicule(isDispo, matricule);
  }
  
  public void getEmploye(java.lang.String idEmploye) throws java.rmi.RemoteException{
    if (ifsCarsService == null)
      _initIfsCarsServiceProxy();
    ifsCarsService.getEmploye(idEmploye);
  }
  
  public void acheterVehiculeDevise(java.lang.String adresseMail, java.lang.String devise) throws java.rmi.RemoteException{
    if (ifsCarsService == null)
      _initIfsCarsServiceProxy();
    ifsCarsService.acheterVehiculeDevise(adresseMail, devise);
  }
  
  public void afficherVehiculeDevise(java.lang.String devise) throws java.rmi.RemoteException{
    if (ifsCarsService == null)
      _initIfsCarsServiceProxy();
    ifsCarsService.afficherVehiculeDevise(devise);
  }
  
  public void louerVehicule(java.lang.String matricule, java.lang.String idEmploye) throws java.rmi.RemoteException{
    if (ifsCarsService == null)
      _initIfsCarsServiceProxy();
    ifsCarsService.louerVehicule(matricule, idEmploye);
  }
  
  public fr.uge.ifsCarsService.Client getClient(java.lang.String adresseMail) throws java.rmi.RemoteException{
    if (ifsCarsService == null)
      _initIfsCarsServiceProxy();
    return ifsCarsService.getClient(adresseMail);
  }
  
  public void afficherVehicule() throws java.rmi.RemoteException{
    if (ifsCarsService == null)
      _initIfsCarsServiceProxy();
    ifsCarsService.afficherVehicule();
  }
  
  public void acheterVehicule(java.lang.String adresseMail) throws java.rmi.RemoteException{
    if (ifsCarsService == null)
      _initIfsCarsServiceProxy();
    ifsCarsService.acheterVehicule(adresseMail);
  }
  
  public boolean ajouterAuPanier(java.lang.String matricule, java.lang.String adresseMail) throws java.rmi.RemoteException{
    if (ifsCarsService == null)
      _initIfsCarsServiceProxy();
    return ifsCarsService.ajouterAuPanier(matricule, adresseMail);
  }
  
  public java.lang.String[] retournerPanier(java.lang.String adresseMail) throws java.rmi.RemoteException{
    if (ifsCarsService == null)
      _initIfsCarsServiceProxy();
    return ifsCarsService.retournerPanier(adresseMail);
  }
  
  public void supprimerPanierClientLigne(java.lang.String matricule, java.lang.String adresseMail) throws java.rmi.RemoteException{
    if (ifsCarsService == null)
      _initIfsCarsServiceProxy();
    ifsCarsService.supprimerPanierClientLigne(matricule, adresseMail);
  }
  
  public double convertCurrency(java.lang.String devise, float prix) throws java.rmi.RemoteException{
    if (ifsCarsService == null)
      _initIfsCarsServiceProxy();
    return ifsCarsService.convertCurrency(devise, prix);
  }
  
  public void inscrireClient(java.lang.String adresseMail, java.lang.String nomClient, java.lang.String prenomClient) throws java.rmi.RemoteException{
    if (ifsCarsService == null)
      _initIfsCarsServiceProxy();
    ifsCarsService.inscrireClient(adresseMail, nomClient, prenomClient);
  }
  
  
}