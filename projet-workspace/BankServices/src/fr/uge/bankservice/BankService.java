package fr.uge.bankservice;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Service Banque 
 * @author Djamila, Hamza et Banou-Priya
 *
 */
public class BankService {
	
	public BankService() {
		
	}
	
	/**
	 * Récupérer le compte d'un client de la base de données 
	 * @param adresseMail
	 * @return
	 */
	public CompteClient getClient(String adresseMail) {

		try {
			Class.forName("com.mysql.jdbc.Driver");
			//Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/BankBDD","root","Sweb54321@");
			Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/BankBDD","root","banou");

			//préparer la requete
			PreparedStatement ps=conn.prepareStatement("select * FROM CompteClient");
			//execution d'une requete de lecture
			ResultSet rs = ps.executeQuery();
			/* Récupération des données du résultat de la requête de lecture */

			while (rs.next()) {
				String mail = rs.getString("adresseMail");
				String nomClient = rs.getString("nomClient");
				String prenomClient = rs.getString("prenomClient");
				float solde  = rs.getFloat("solde");

				if(adresseMail.equals(mail)) {
					System.out.println("Client trouvé ");
					return new CompteClient(adresseMail, nomClient, prenomClient, solde);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		System.out.println("Client n est pas dans la base");
		return null;
	}
	/**
	 * Vérifier le solde d'un client 
	 * @param adresseMail
	 * @param prix
	 * @return
	 */
	public boolean payement (String adresseMail, float prix) {
		CompteClient client;
		client = getClient(adresseMail);
		if(client == null ) {
			System.out.println("Le Client n'appartient pas a la banque ");
			return false;
		}
		else {
			if(prix > client.getSolde()) {
				System.out.println("solde insuffisant");
				return false;
			}else {
				client.setSolde(client.getSolde()-prix);
				updateSolde(adresseMail, client.getSolde()-prix);
				System.out.println("vous avez reussi a payer");
				return true;
			}
		}
	
	}
	
	/**
	 * Méthode interne. Mettre à jour la base de données d'un compte client.
	 * @param adresseMailClient
	 * @param newSolde
	 */
	public void updateSolde(String adresseMailClient, float newSolde ) {
		
		try {
			Class.forName("com.mysql.jdbc.Driver");
			//Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/BankBDD","root","Sweb54321@");
			Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/BankBDD","root","banou");

			//préparer la requete
			PreparedStatement pss=conn.prepareStatement("update CompteClient SET solde = " + newSolde + "where adresseMail = \""+adresseMailClient + "\"");
			pss.executeUpdate(); 
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	
	}

}
