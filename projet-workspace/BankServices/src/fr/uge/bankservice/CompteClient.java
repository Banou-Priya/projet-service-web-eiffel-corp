package fr.uge.bankservice;

/**
 * Classe représentant le compte d'un Client
 * @author Djamila, Hamza et Banou-Priya
 *
 */

public class CompteClient {
	private String adresseMail;
	private String nomClient;
	private String prenomClient;
	private float solde;

	public CompteClient(String adresseMail, String nomClient, String prenomClient, float solde) {
		super();
		this.adresseMail = adresseMail;
		this.nomClient = nomClient;
		this.prenomClient = prenomClient;
		this.solde = solde;
	}

	public CompteClient() {

	}

	public String adresseMail() {
		return adresseMail;
	}

	public void setAdresseMail(String adresseMail) {
		this.adresseMail = adresseMail;
	}

	public String getNomClient() {
		return nomClient;
	}

	public void setNomClient(String nomClient) {
		this.nomClient = nomClient;
	}

	public String getprenomClient() {
		return prenomClient;
	}

	public void setprenomClient(String prenomClient) {
		this.prenomClient = prenomClient;
	}

	public float getSolde() {
		return solde;
	}

	public void setSolde(float solde) {
		this.solde = solde;
	}
	
	@Override
	public String toString() {
		return "CompteClient [adresseMail=" + adresseMail + ", nomClient=" + nomClient + ", prenomClient="
				+ prenomClient + ", solde=" + solde + "]";
	}
}
