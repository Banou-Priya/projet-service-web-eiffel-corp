package fr.uge.location;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface IEmploye extends Remote {

	float getBudget() throws RemoteException;
	void setBudget(float budget) throws RemoteException;
	String getIdEmploye() throws RemoteException;
	void setIdEmploye(String idEmploye) throws RemoteException;	
	
}
