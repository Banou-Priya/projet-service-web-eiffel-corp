package fr.uge.location;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

/**
 * Cette classe permet faire la location de Vehicule
 * @author Djamila, Hamza  et Banou-Priya
 *
 */
public class IfsCars extends UnicastRemoteObject implements IIfsCars {
	
	LinkedList<String> linkedList;
	Map<String, LinkedList<String>> map;

	public IfsCars() throws RemoteException {
		super();
		map = new HashMap<String, LinkedList<String>>();
		linkedList = new LinkedList<String>();

	}

	/**
	 * Permet d'établir la connexion à la base de données
	 */
	public Connection connectToBDD() throws RemoteException{
		try {
			try {
				Class.forName("com.mysql.jdbc.Driver");
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
			Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/LocationBDD","root","banou");
			//Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/EiffelCorpBDD","root","Sweb54321@");
			return conn;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;

	}
	
	/**
	 * Permet de rajouter un employé dans la liste d'attente
	 */
	@Override
	public void subscribe(String matricule, String idEmploye) throws RemoteException {
		LinkedList<String> l = new LinkedList<String>();
		l.add(idEmploye);
		map.put(matricule, l);
		//ajouter un client dans la liste 
		System.out.println("Vous allez etre en attente pour ce vehicule");
	}

	/**
	 * Permet à un client de louer un véhicule si celle-ci est disponible
	 */
	public boolean louerVehicule(String matricule, String idEmploye) throws RemoteException {
		Connection conn = connectToBDD();

		//vehicule existe dans la base de donnees ??
		Vehicule vehicule = getVehicule(matricule);
		if(vehicule ==null) {
			System.out.println("Véhicule n existe pas ");
			return false;
		}
		//employe existe dans la base ?
		Employe employe = getEmploye(idEmploye);
		if(employe ==null) {
			System.out.println("Employe n existe pas ");
			return false;
		}

		if ( vehicule.isDispo()  && (employe.getBudget() < vehicule.getPrix()) ) {
			System.out.println("Le véhicule est disponible mais vous n'avez pas le budget ");
			return false;
		}	
		else if (vehicule.isDispo() == false ){
			System.out.println("Le véhicule n'est pas disponible");
			subscribe(vehicule.getMatricule(), employe.getIdEmploye());
			//rajouter une liste FIFO et le mettre dedans 
			return false;
		}
		else if ( vehicule.isDispo()  && (employe.getBudget() >= vehicule.getPrix()) ) {
			System.out.println("Vous avez réussi à louer le véhicule");
			vehicule.setDispo(false);
			vehicule.setIdEmploye(idEmploye); 
			System.out.println("le véhicule loué a " + vehicule.getIdEmploye());
			
			//update employe 
			updateBudgetEmploye((employe.getBudget() - vehicule.getPrix()), idEmploye);
			
			//update vehicule 
			updateDispoVehicule(false, idEmploye, matricule);
			
			//vehicule.vLouerPar(client);
			return true;
		}
		return false;
	}

	/**
	 * Cette méthode sert à afficher les véhicules de location
	 */
	@Override
	public void afficherVehiculesLocation() throws RemoteException {

		try {
			//Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/location","root","banou");
			Connection conn = connectToBDD();

			//préparer la requete
			PreparedStatement ps=conn.prepareStatement("select * FROM Vehicule");
			//execution d'une requete de lecture
			ResultSet rs = ps.executeQuery();
			/* Récupération des données du résultat de la requête de lecture */

			while (rs.next()) {
				String matricule = rs.getString("pk_immatriculation");
				String modele = rs.getString("modele");
				String marque = rs.getString("marque");
				String couleur = rs.getString("couleur");
				int prix = rs.getInt("prixLocation");
				boolean isDispo = rs.getBoolean("is_dispo");
				String fk_idEmploye = rs.getString("fk_idEmploye");

				System.out.println("Vehicule : "+matricule+" " +modele+" "+ " " + marque + " "+ couleur + " " + prix );
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	/**
	 * Méthode interne pour récupérer un véhicule si il est dans la base de données
	 * Renvoie null si ce véhicule n'existe pas
	 */
	public Vehicule getVehicule(String matricule) throws RemoteException {

		try {
			//Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/location","root","banou");
			Connection conn = connectToBDD();

			//préparer la requete
			PreparedStatement ps=conn.prepareStatement("select * FROM Vehicule");
			//execution d'une requete de lecture
			ResultSet rs = ps.executeQuery();
			/* Récupération des données du résultat de la requête de lecture */

			while (rs.next()) {
				String pk_immatriculation = rs.getString("pk_immatriculation");
				String modele = rs.getString("modele");
				String marque = rs.getString("marque");
				String couleur = rs.getString("couleur");
				int prix = rs.getInt("prixLocation");
				boolean isDispo = rs.getBoolean("is_dispo");
				boolean louerAumoinsUneFois = rs.getBoolean("louerAuMoinsUneFois");
				String idEmploye = rs.getString("fk_idEmploye");

				if(pk_immatriculation.equals(matricule)) {
					System.out.println("véhicule trouvé ");
					
					System.out.println("Vehicule : "+matricule+" " +modele+" " + marque + " "+ couleur + " " + prix );
					return new Vehicule(pk_immatriculation, modele, marque, couleur , isDispo, prix, louerAumoinsUneFois, idEmploye);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		System.out.println("Véhicule n est pas dans la base");
		return null;
	}

	/**
	 * Méthode interne pour vérfier si un employé est dans la base de données
	 * Renvoie null sinon
	 */
	@Override
	public Employe getEmploye(String id) throws RemoteException {

		try {
			Connection conn = connectToBDD();

			//préparer la requete
			PreparedStatement ps=conn.prepareStatement("select * FROM Employe");
			//execution d'une requete de lecture
			ResultSet rs = ps.executeQuery();
			/* Récupération des données du résultat de la requête de lecture */

			while (rs.next()) {
				String idEmploye = rs.getString("idEmploye");
				String nom = rs.getString("nomEmploye");
				String prenom = rs.getString("prenomEmploye");
				int budget = rs.getInt("budgetEmploye");

				if(id.equals(idEmploye)) {
					System.out.println("employé trouvé ");
					return new Employe(idEmploye, nom, prenom, budget);
				}
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		System.out.println("Employe n est pas dans la base");
		return null;
	}
	
	/**
	 * Méthode interne pour mettre à jour le solde d'un employé
	 */
	@Override
	public void updateBudgetEmploye(float budget, String idEmploye) throws RemoteException {
		
			Connection conn;
			try {
				conn = connectToBDD();
				//préparer la requete
				PreparedStatement pss=conn.prepareStatement("update Employe SET budgetEmploye = " + budget + "where idEmploye = \""+idEmploye + "\"");
				pss.executeUpdate(); 
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}
	
	
	
	/**
	 * Mettre à jour la base de données du véhicule
	 * @param isDispo
	 * @param matricule
	 */
	public void updateDispoVehicule(boolean isDispo, String idEmploye, String matricule) {

		Connection conn;
		try {
			conn = connectToBDD();
			//préparer la requete
			System.out.println("update Vehicule SET is_dispo ="+ isDispo + ", fk_idEmploye= \""+ idEmploye+"\" , louerAuMoinsUnefois=true where pk_immatriculation= "+"\"" +matricule+ "\"");
			PreparedStatement pss=conn.prepareStatement("update Vehicule SET is_dispo ="+ isDispo + ", fk_idEmploye= \""+ idEmploye+"\" , louerAuMoinsUnefois=true where pk_immatriculation= "+"\"" +matricule+ "\"");
			pss.executeUpdate();
			
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	/**
	 * Permet d'ajouter une note à un véhicule rendu
	 * @param matricule
	 * @param idEmploye
	 * @param notesEmploye
	 */
	public void ajoutNotesEmploye(String matricule, String idEmploye, String notesEmploye) {
		Connection conn;
		try {
			conn = connectToBDD();
			//préparer la requete
			System.out.println("Votre note va être ajouté ");
			System.out.println("INSERT INTO NotesEmploye (fk_idEmploye, fk_immatriculation, noteEmploye ) VALUES ( \"" + idEmploye + "\", \""+ matricule +"\", \""+ notesEmploye +"\" )");
			PreparedStatement pss=conn.prepareStatement("INSERT INTO NotesEmploye (fk_idEmploye, fk_immatriculation, noteEmploye ) VALUES ( \"" + idEmploye + "\", \""+ matricule +"\", \""+ notesEmploye +"\" )");
			pss.executeUpdate(); 
		} catch (RemoteException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	
	/**
	 * méthode appelée localement quand la donnée change de valeur
	 * @param matricule
	 * @throws RemoteException
	 */
	public void notifyClient(String matricule) throws RemoteException {
		System.out.println("Notifier le client suivant ");
		// on informe tous les observateurs distants de la nouvelle valeur
		for (Map.Entry mapentry : map.entrySet()) {
	       
	           if(mapentry.getKey().equals(matricule)) {
	        	   LinkedList l = (LinkedList) mapentry.getValue();
	        	   String idEmploye = l.getFirst().toString();
	        	   System.out.println("Le véhicule est disponible et on vous l'a loué !!");
	        	   louerVehicule(matricule, idEmploye);
	        	   l.poll();
	     
	           }
	        }
	}

	/**
	 * Permet à un client de rendre le véhicule
	 */
	@Override
	public void rendreVehicule(String matricule, String notesEmploye) throws RemoteException {
		
		Vehicule vehicule = getVehicule(matricule);
		if(vehicule == null) {
			System.out.println("Ce vehicule n'existe pas");
			return;
		}
		else if(!vehicule.getMatricule().equals(matricule)) {
			System.out.println("Ce véhicule n est pas dans la base");
			return;
		}
		else {
			if(notesEmploye != null) {
				ajoutNotesEmploye(vehicule.getMatricule(), vehicule.getIdEmploye(), notesEmploye);
			}
			updateDispoVehicule(true, null ,matricule);
			System.out.println("Le véhicule a été rendu ");
		}
		notifyClient(matricule); // notifier la derniere personne en attente
	}
	
}
