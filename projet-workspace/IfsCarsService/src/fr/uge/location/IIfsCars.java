package fr.uge.location;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.sql.Connection;

public interface IIfsCars extends Remote {

	boolean louerVehicule(String matricule, String idEmploye) throws RemoteException;
	void rendreVehicule(String matricule, String notesClient) throws RemoteException;
	void afficherVehiculesLocation() throws RemoteException;
	Vehicule getVehicule(String matricule) throws RemoteException;
	IEmploye getEmploye(String idEmploye) throws RemoteException;
	Connection connectToBDD() throws RemoteException;
	void updateBudgetEmploye(float budget, String idEmploye) throws RemoteException;
	public void subscribe(String matricule, String idEmploye) throws RemoteException;
	

	
	
	
	

}
