package fr.uge.location;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class Vehicule extends UnicastRemoteObject implements IVehicule{  

	private final String matricule;
	private String modele;
	private String marque;
	private String couleur;
	private boolean isDispo;
	private float prix;
	private String idEmploye;
	private boolean louerAuMoinsUneFois;

	public Vehicule(String matricule, String modele, String marque,  String couleur, boolean isDispo, float prix, boolean louerAuMoinsUneFois,  String idEmploye) throws RemoteException {
		super();
		this.matricule = matricule;
		this.marque = marque;
		this.couleur = couleur;
		this.modele = modele;
		this.isDispo = isDispo;
		this.prix = prix;
		this.louerAuMoinsUneFois = louerAuMoinsUneFois;
		this.idEmploye = idEmploye; 
	}

	public boolean isLouerAuMoinsUneFois() {
		return louerAuMoinsUneFois;
	}

	public void setLouerAuMoinsUneFois(boolean louerAuMoinsUneFois) {
		this.louerAuMoinsUneFois = louerAuMoinsUneFois;
	}

	@Override
	public String getIdEmploye() throws RemoteException{
		return this.idEmploye; 
	}

	@Override
	public void setIdEmploye(String idEmploye) throws RemoteException {
		this.idEmploye = idEmploye;
	}

	@Override
	public boolean isDispo() throws RemoteException {
		return isDispo;
	}

	public void setDispo(boolean isDispo) throws RemoteException {
		this.isDispo = isDispo;
	}

	public float getPrix() {
		return prix;
	}

	public String getMatricule() {
		return matricule;
	}

	public void setPrix(float prix) {
		this.prix = prix;
	}
	
	@Override
	public String toString() {
		return "Vehicule [matricule=" + matricule + ", modele=" + modele + ", marque=" + marque + ", couleur=" + couleur
				+ ", isDispo=" + isDispo + ", prix=" + prix + ", idEmploye=" + idEmploye + ", clientList=" + "]";
	}
	
}
