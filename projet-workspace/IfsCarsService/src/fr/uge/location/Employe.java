package fr.uge.location;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class Employe extends UnicastRemoteObject implements IEmploye {
	private String idEmploye;
	private String nomEmploye;
	private String prenomEmploye;
	private float budget;

	public Employe(String idEmploye, String nomEmploye, String prenomEmploye, float budget) throws RemoteException {
		super();
		this.idEmploye = idEmploye;
		this.nomEmploye = nomEmploye;
		this.prenomEmploye = prenomEmploye;
		this.budget = budget;
	}

	public String getPrenomEmploye() throws RemoteException {
		return prenomEmploye;
	}

	public void setPrenomEmploye(String prenomEmploye) throws RemoteException {
		this.prenomEmploye = prenomEmploye;
	}

	public String getIdEmploye() throws RemoteException {
		return idEmploye;
	}

	public void setIdEmploye(String idEmploye) throws RemoteException {
		this.idEmploye = idEmploye;
	}

	public String getNomEmploye() {
		return nomEmploye;
	}

	public void setNomEmploye(String nomEmploye) {
		this.nomEmploye = nomEmploye;
	}

	public float getBudget() throws RemoteException {
		return budget;
	}

	@Override
	public void setBudget(float budget) throws RemoteException {
		this.budget = budget;
	}

	@Override
	public String toString() {
		return "Employe [idEmploye=" + idEmploye + ", nomEmploye=" + nomEmploye + ", prenomEmploye=" + prenomEmploye
				+ ", budget=" + budget + "]";
	}

}
