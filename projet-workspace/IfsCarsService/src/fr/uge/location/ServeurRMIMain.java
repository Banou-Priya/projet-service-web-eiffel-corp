package fr.uge.location;

import java.rmi.Naming;
import java.rmi.registry.LocateRegistry;

/**
 * Cette classe permet de lancer le serveur RMI
 * @author Djamila, Hamza et Banou-Priya
 *
 */
public class ServeurRMIMain {

	public static void main(String[] args) {

		try {
			/*
			 LocateRegistry.createRegistry(1099);
			 IBib bib = new Bib();
			 Naming.rebind("rmi://localhost/BibService", bib); 
			 */
			LocateRegistry.createRegistry(1099);
			IIfsCars ifsCars = new IfsCars();
			Naming.rebind("rmi://localhost/IfsCarsService", ifsCars);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
