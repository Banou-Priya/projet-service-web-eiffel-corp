package fr.uge.location;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface IVehicule extends Remote { 
	
	boolean isDispo() throws RemoteException;
	void setDispo(boolean b) throws RemoteException; 
	float getPrix() throws RemoteException; 
	String getIdEmploye() throws RemoteException;
	void setIdEmploye(String idEmploye) throws RemoteException;
	
	
}
