package fr.uge.ifsCarsService;      
                            
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.rpc.ServiceException;

import com.currencysystem.webservices.currencyserver.CurncsrvReturnRate;
import com.currencysystem.webservices.currencyserver.CurrencyServerLocator;
import com.currencysystem.webservices.currencyserver.CurrencyServerSoap;

import fr.uge.bankservice.*;
import fr.uge.location.IIfsCars;


public class IfsCarsService {
	
	private IIfsCars ifcars;

	public IfsCarsService() throws MalformedURLException, RemoteException, NotBoundException {
		super();	
		//le client de RMI 
		ifcars  = (IIfsCars) Naming.lookup("rmi://localhost/IfsCarsService");  
	}
	
	/**
	 * Cette méthode sert à afficher les véhicules qui peuvent être louer
	 */
	public void afficherVehiculesPourLouer() {
		try {
			ifcars.afficherVehiculesLocation();
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}
	/**
	 * Vérifier si un employé existe dans la base de données
	 * @param idEmploye
	 */
	public void getEmploye(String idEmploye) {
		try {
			ifcars.getEmploye(idEmploye);
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Permet un employé de louer un véhicule
	 * @param matricule
	 * @param idEmploye
	 */
	public void louerVehicule(String matricule, String idEmploye) {
		try {
			ifcars.louerVehicule(matricule, idEmploye);
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}
	/**
	 * Permet à un employé de rendre le véhicule
	 * @param matricule
	 * @param notesClient
	 */
	public void rendreVehicule(String matricule, String notesClient) {
		try {
			ifcars.rendreVehicule(matricule, notesClient);
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Permet à un employé d'afficher les véhicules à vendre
	 */
	public void afficherVehicule() throws RemoteException {

		try {

			Class.forName("com.mysql.jdbc.Driver");
			//Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/EiffelCorpBDD","root","Sweb54321@");
			Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/LocationBDD","root","banou");

			//préparer la requete
			PreparedStatement ps=conn.prepareStatement("select * FROM Vehicule where louerAuMoinsUneFois=true"); 
			//execution d'une requete de lecture
			ResultSet rs = ps.executeQuery();
			/* Récupération des données du résultat de la requête de lecture */

			while (rs.next()) {
				String matricule = rs.getString("pk_immatriculation");
				String modele = rs.getString("modele");
				String marque = rs.getString("marque");
				String couleur = rs.getString("couleur");
				int prix = rs.getInt("prixVente");
				
				System.out.println("Voiture : "+matricule+" "+modele+" " + " " + marque + " "+ couleur + " " + prix + " ");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

	}
	/**
	 * Permet d'afficher tous les véhicules avec la devise souhaité 
	 * @param devise
	 * @throws RemoteException
	 */
	public void afficherVehiculeDevise(String devise) throws RemoteException {

		try {

			Class.forName("com.mysql.jdbc.Driver");
			//Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/EiffelCorpBDD","root","Sweb54321@");
			Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/LocationBDD","root","banou");

			//préparer la requete
			PreparedStatement ps=conn.prepareStatement("select * FROM Vehicule "); 
			//execution d'une requete de lecture
			ResultSet rs = ps.executeQuery();
			/* Récupération des données du résultat de la requête de lecture */

			while (rs.next()) {
				String matricule = rs.getString("pk_immatriculation");
				String modele = rs.getString("modele");
				String marque = rs.getString("marque");
				String couleur = rs.getString("couleur");
				float prix = rs.getFloat("prixVente");
				
				prix = (float) convertCurrency("USD", prix);
				System.out.println("Voiture : " + matricule+" "+modele+" "+ " " + marque + " "+ couleur + " " + prix + devise);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

	}
	
	/**
	 * Vérifier si un employé existe dans la base de données
	 * @param matricule
	 */
	public Vehicule getVehicule(String matricule) throws RemoteException {

		try {
			Class.forName("com.mysql.jdbc.Driver");
			//Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/EiffelCorpBDD","root","Sweb54321@");
			Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/LocationBDD","root","banou");

			//préparer la requete
			PreparedStatement ps=conn.prepareStatement("select * FROM Vehicule");
			//execution d'une requete de lecture
			ResultSet rs = ps.executeQuery();
			/* Récupération des données du résultat de la requête de lecture */

			while (rs.next()) {
				String pk_immatriculation = rs.getString("pk_immatriculation");
				String modele = rs.getString("modele");
				String marque = rs.getString("marque");
				String couleur = rs.getString("couleur");
				int prix = rs.getInt("prixLocation");
				boolean isDispo = rs.getBoolean("is_dispo");
				boolean louerAumoinsUneFois = rs.getBoolean("louerAuMoinsUneFois");
				String idEmploye = rs.getString("fk_idEmploye");

				if(pk_immatriculation.equals(matricule)) {
					System.out.println("véhicule trouvé ");
					
					System.out.println("Vehicule : "+matricule+" " +modele+" " + marque + " "+ couleur + " " + prix );
					return new Vehicule(pk_immatriculation, modele, marque, couleur , prix, isDispo, louerAumoinsUneFois, idEmploye);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Véhicule n est pas dans la base");
		return null;
	}

	/**
	 * Permet à un client de s'inscrire
	 * @param adresseMail
	 * @param nomClient
	 * @param prenomClient
	 */
	public void inscrireClient(String adresseMail,String nomClient, String prenomClient) {
		try {
			Client client = getClient(adresseMail);

			if(client == null || ! client.getAdresseMail().equals(adresseMail)) {
				Class.forName("com.mysql.jdbc.Driver");

				Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/VenteBDD","root","banou");
				//Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/VenteBDD","root","Sweb54321@");
				
				PreparedStatement ps=conn.prepareStatement("insert into Client (adresseMailClient, nomClient, prenomClient) values ( \"" +adresseMail + "\",\"" + nomClient + "\",\"" + prenomClient + "\")");

				//execution d'une requete de lecture
				ps.executeUpdate();
				System.out.println("Creation de compte reussi");
			}
			else {
				System.out.println("Vous etes deja inscrit");
			}
		} catch (RemoteException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Cette méthode permet à un client d'acheter un véhicule
	 * @param adresseMail
	 * @throws RemoteException
	 */
	public void acheterVehicule(String adresseMail) throws RemoteException {  
		try {
			//employe existe dans la base ?
			Client client = getClient(adresseMail);
			if(client ==null) {
				System.out.println("Vous n etes pas inscrit  ");
				return;
			}
			else {
				String [] tabPanier = retournerPanier(adresseMail);
				System.out.println(tabPanier);
				List listPanier = new ArrayList<String>();

				if(tabPanier == null) {
					System.out.println("Le panier est vide ");
					return;
				}

				for(int i=0; i<tabPanier.length;i++) {
					listPanier.add(tabPanier[i]);
				}
				if(listPanier.isEmpty()) {
					System.out.println("Votre panier est vide");
					return;
				}
				else {
					float prixTotal = 0;

					for(int i=0; i<listPanier.size(); i++ ) { //pour chaque vehicule de mon panier

						//verifier si dispo dans le vehicule 

						Class.forName("com.mysql.jdbc.Driver");
						//Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/VenteBDD","root","Sweb54321@"); 
						Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/LocationBDD","root","banou"); 
						//préparer la requete
						PreparedStatement ps=conn.prepareStatement("select * FROM Vehicule where pk_immatriculation= \""+ listPanier.get(i) + "\" and is_dispo=true"  );
						//execution d'une requete de lecture
						ResultSet rs = ps.executeQuery();
						/* Récupération des données du résultat de la requête de lecture */

						while (rs.next()) {
							float prixVehicule = rs.getFloat("prixVente");
							prixTotal = prixTotal + prixVehicule;
							supprimerPanierClientLigne(listPanier.get(i).toString(), adresseMail);
						}
					}
					//une fois qu'on a parcouru tout le panier on va payer 

					System.out.println("Vous devez payer "+ prixTotal);
					BankService bankService = new BankServiceServiceLocator().getBankService();
					((BankServiceSoapBindingStub)bankService).setMaintainSession(true);
					
					if(bankService.payement(adresseMail, prixTotal) == false) {
						System.out.println("La banque a refusé le paiement");
						return;
					}
								
					System.out.println("Vous avez réussi à acheter le véhicule");
					//suppression des vehicules dans la base si payement OK
					for(int i=0; i<listPanier.size(); i++) {
						deleteVehicule(false, listPanier.get(i).toString());
					}
				}
			}
		} catch (RemoteException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ServiceException e) {
			e.printStackTrace();
		} 
	}
	
	/**
	 * Permet à un client d'acheter son panier avec la devise qu'il a choisi
	 * @param adresseMail
	 * @param devise
	 * @throws RemoteException
	 */
	public void acheterVehiculeDevise(String adresseMail, String devise) throws RemoteException {  
		try {
			//employe existe dans la base ?
			Client client = getClient(adresseMail);
			if(client ==null) {
				System.out.println("Vous n etes pas inscrit  ");
				return;
			}
			else {
				String [] tabPanier = retournerPanier(adresseMail);
				System.out.println(tabPanier);
				List listPanier = new ArrayList<String>();

				if(tabPanier == null) {
					System.out.println("Le panier est vide ");
					return;
				}
				for(int i=0; i<tabPanier.length;i++) {
					listPanier.add(tabPanier[i]);
				}
				if(listPanier.isEmpty()) {
					System.out.println("Votre panier est vide");
					return;
				}
				else {
					float prixTotal = 0;

					for(int i=0; i<listPanier.size(); i++ ) { //pour chaque vehicule de mon panier

						//verifier si dispo dans le vehicule 

						Class.forName("com.mysql.jdbc.Driver");
						//Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/VenteBDD","root","Sweb54321@"); 
						Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/VenteBDD","root","banou"); 

						//préparer la requete
						PreparedStatement ps=conn.prepareStatement("select * FROM Vehicule where pk_immatriculation= \""+ listPanier.get(i) + "\""  );

						//execution d'une requete de lecture
						ResultSet rs = ps.executeQuery();
						/* Récupération des données du résultat de la requête de lecture */

						while (rs.next()) {
							float prixVehicule = rs.getFloat("prixVente");
							prixTotal = prixTotal + prixVehicule;
						}
						supprimerPanierClientLigne(listPanier.get(i).toString(), adresseMail);

					}
					//une fois qu'on a parcouru tout le panier on va payer 

					System.out.println("Vous devez payer "+ prixTotal);
					BankService bankService = new BankServiceServiceLocator().getBankService();
					((BankServiceSoapBindingStub)bankService).setMaintainSession(true);
					
					bankService.payement(adresseMail, prixTotal);
								
					System.out.println("Vous avez réussi à acheter le véhicule");
					System.out.println("Vous avez payé : " + convertCurrency(devise, prixTotal) + devise);
					//suppression des vehicules dans la base si payement OK
					for(int i=0; i<listPanier.size(); i++) {
						deleteVehicule(false, listPanier.get(i).toString());
					}
				}
			}
		} catch (RemoteException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ServiceException e) {
			e.printStackTrace();
		} 
	}
	
	/**
	 * Cette méthode permet de convertir une monnaie d'euro à la devise souhaité
	 * @param devise
	 * @param prix
	 * @return
	 */
	public double convertCurrency(String devise, double prix) {
        try {
        	
        	CurncsrvReturnRate curncsrvReturnRate = CurncsrvReturnRate.curncsrvReturnRateNumber;
        	CurrencyServerSoap service = new CurrencyServerLocator().getCurrencyServerSoap();
            double prixConverti = (double) service.convert("","EUR", devise, prix, true, "", curncsrvReturnRate, "", "");
            return prixConverti;
           
        } catch (ServiceException e) {
            e.printStackTrace();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
		return prix;
    }

	/**
	 * Méthode interne pour récupérer un client dans la base de données
	 * @param adresseMail
	 * @return
	 * @throws RemoteException
	 */
	public Client getClient(String adresseMail) throws RemoteException {

		try {
			Class.forName("com.mysql.jdbc.Driver");
			//Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/EiffelCorpBDD","root","Sweb54321@"); 
			Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/VenteBDD","root","banou"); 

			//préparer la requete
			PreparedStatement ps=conn.prepareStatement("select * FROM Client");
			//execution d'une requete de lecture
			ResultSet rs = ps.executeQuery();
			/* Récupération des données du résultat de la requête de lecture */

			while (rs.next()) {
				String mail = rs.getString("adresseMailClient");
				String nomClient = rs.getString("nomClient");
				String prenomClient = rs.getString("prenomClient");

				if(adresseMail.equals(mail)) {
					System.out.println("Client trouvé ");
					return new Client(adresseMail, nomClient, prenomClient);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		System.out.println("Client n est pas dans la base");
		return null;
	}

	/**
	 * Méthode interne pour supprimer un véhicule de la base de données
	 * @param isDispo
	 * @param matricule
	 * @throws RemoteException
	 */
	public void deleteVehicule(boolean isDispo, String matricule)throws RemoteException {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			//Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/LocationBDD","root","Sweb54321@");
			Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/LocationBDD","root","banou");
			
			//préparer la requete
			PreparedStatement pss=conn.prepareStatement("delete from Vehicule where pk_immatriculation = \"" + matricule+ "\""); 
			pss.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Cette méthode permet à un client d'ajouter un véhicule dans son panier
	 * @param matricule
	 * @param adresseMail
	 * @return
	 */
	public boolean ajouterAuPanier(String matricule, String adresseMail) {

		try {
			Vehicule vehicule = getVehicule(matricule);
			if(vehicule ==null) {
				System.out.println("Véhicule n existe pas ");
				return false;
			}
			//employe existe dans la base ?
			Client client = getClient(adresseMail);
			if(client ==null) {
				System.out.println("Vous n etes pas inscrit");
				return false;
			}
			
			if(vehicule.isLouerAuMoinsUneFois()==false) {
				System.out.println("le vehicule n'est pas a vendre");
				return false; 
			}
			
			Class.forName("com.mysql.jdbc.Driver");
			//Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/VenteBDD","root","Sweb54321@");
			Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/VenteBDD","root","banou");
			//préparer la requete
			PreparedStatement pss=conn.prepareStatement("INSERT INTO PanierClients (fk_adresseMailClient, fk_immatriculation) VALUES (\"" + adresseMail + "\", \""+ matricule  +"\")");
			pss.executeUpdate(); 
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * Méthode interne pour retourner le panier d'un client de la base de données 
	 * @param adresseMail
	 * @return
	 */
	public String[] retournerPanier(String adresseMail){

		List<String> listPanier= new ArrayList<String>();
		try {
			Class.forName("com.mysql.jdbc.Driver");
			//Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/VenteBDD","root","Sweb54321@");
			Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/VenteBDD","root","banou");

			//préparer la requete
			System.out.println("select fk_immatriculation FROM PanierClients where fk_adresseMailClient = \"" + adresseMail + "\"");
			PreparedStatement ps=conn.prepareStatement("select fk_immatriculation FROM PanierClients where fk_adresseMailClient = \"" + adresseMail + "\"");
			//execution d'une requete de lecture
			ResultSet rs = ps.executeQuery();
			/* Récupération des données du résultat de la requête de lecture */
			while (rs.next()) {
				String matricule = rs.getString("fk_immatriculation");
				listPanier.add(matricule);
			}
			System.out.println(listPanier.toString());

		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String[] tabPanier = new String[listPanier.size()];
		for(int i=0; i<listPanier.size(); i++) {
			tabPanier[i] = listPanier.get(i); 
			System.out.println(tabPanier[0]);

		}
		System.out.println(tabPanier);
		return tabPanier;
	}

	/**
	 * Méthode interne pour supprimer une ligne du panier d'un client
	 * @param matricule
	 * @param adresseMail
	 */
	public void supprimerPanierClientLigne(String matricule, String adresseMail) {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			//Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/VenteBDD","root","Sweb54321@");
			Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/VenteBDD","root","banou");

			//préparer la requete
			System.out.println("delete from PanierClients where fk_immatriculation = \"" + matricule+ "\", " + "fk_adresseMailClient = \"" + adresseMail +"\"");
			PreparedStatement pss=conn.prepareStatement("delete from PanierClients where fk_immatriculation = \"" + matricule+ "\" and  " + "fk_adresseMailClient = \"" + adresseMail +"\"");
			pss.executeUpdate();

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}


}
