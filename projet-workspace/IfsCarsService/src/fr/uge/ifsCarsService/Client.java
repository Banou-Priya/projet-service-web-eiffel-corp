package fr.uge.ifsCarsService;

import java.rmi.RemoteException;
import java.util.List;

public class Client {

	private String adresseMail;
	private String nomClient;
	private String prenomClient;

	public Client(String adresseMail, String nomClient, String prenomClient) throws RemoteException  {
		super();
		this.adresseMail = adresseMail;
		this.nomClient = nomClient;
		this.prenomClient = prenomClient;
	}

	public Client() {

	}

	public String getAdresseMail() throws RemoteException {
		return adresseMail;
	}

	public void setAdresseMail(String adresseMail) throws RemoteException {
		this.adresseMail = adresseMail;
	}

	public String getNomClient() throws RemoteException {
		return nomClient;
	}

	public void setNomClient(String nomClient) throws RemoteException{
		this.nomClient = nomClient;
	}

	public String getprenomClient() throws RemoteException{
		return prenomClient;
	}

	public void setprenomClient(String prenomClient) throws RemoteException {
		this.prenomClient = prenomClient;
	}

	@Override
	public String toString() {
		return "Client [adresseMail=" + adresseMail + ", nomClient=" + nomClient + ", prenomClient=" + prenomClient
				+ "]";
	}

}
