package fr.uge.ifsCarsService;

import java.rmi.RemoteException;


public class Vehicule {

	private final String matricule;
	private String modele;
	private String marque;
	private String couleur;
	private boolean isDispo;
	private float prix;
	private String idEmploye;
	boolean louerAuMoinsUneFois;

	public Vehicule(String matricule, String modele, String marque,  String couleur, float prix, boolean isDispo, boolean louerAuMoinsUneFois , String idEmploye) throws RemoteException {
		super();
		this.matricule = matricule;
		this.marque = marque;
		this.couleur = couleur;
		this.modele = modele;
		//this.notesClients = notesClients;
		this.isDispo = isDispo;
		this.prix = prix;
		this.louerAuMoinsUneFois = louerAuMoinsUneFois;
		this.idEmploye = idEmploye; 
	}
	
	public Vehicule() {
		this.matricule = "";
		
	}
	
	public boolean isLouerAuMoinsUneFois() {
		return louerAuMoinsUneFois;
	}

	public void setLouerAuMoinsUneFois(boolean louerAuMoinsUneFois) {
		this.louerAuMoinsUneFois = louerAuMoinsUneFois;
	}
	
	public String getIdEmploye() {
		return idEmploye;
	}

	public void setIdEmploye(String idEmploye) {
		this.idEmploye = idEmploye;
	}

	public boolean isDispo() throws RemoteException {
		return isDispo;
	}

	public void setDispo(boolean isDispo) throws RemoteException {
		this.isDispo = isDispo;
	}

	public float getPrix() {
		return prix;
	}

	public String getMatricule() {
		return matricule;
	}

	public void setPrix(float prix) {
		this.prix = prix;
	}

	@Override
	public String toString() {
		return "Vehicule [matricule=" + matricule + ", modele=" + modele + ", marque=" + marque + ", couleur=" + couleur
				+ ", isDispo=" + isDispo + ", prix=" + prix + ", idEmploye=" + idEmploye + ", louerAuMoinsUneFois="
				+ louerAuMoinsUneFois + "]";
	}

}
