/**
 * BankService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package fr.uge.bankservice;

public interface BankService extends java.rmi.Remote {
    public boolean payement(java.lang.String adresseMail, float prix) throws java.rmi.RemoteException;
    public void updateSolde(java.lang.String adresseMailClient, float newSolde) throws java.rmi.RemoteException;
}
