CREATE DATABASE BankBDD; 

USE BankBDD; 

CREATE TABLE CompteClient (
adresseMail varchar(40) PRIMARY KEY,
nomClient varchar(20),
prenomClient varchar(20),
solde float DEFAULT NULL
);

INSERT INTO CompteClient (adresseMail, nomClient, prenomClient, solde) VALUES ('alonetonight42@gmail.com', 'ERRIAHI', 'Omar', 100000);
INSERT INTO CompteClient (adresseMail, nomClient, prenomClient, solde) VALUES ('jerando.ham@gmail.com', 'JERANDO', 'Hamza', 100000);
INSERT INTO CompteClient (adresseMail, nomClient, prenomClient, solde) VALUES ('banou.priya@gmail.com', 'SINNATAMBY', 'Banou-Priya ', 100000);
INSERT INTO CompteClient (adresseMail, nomClient, prenomClient, solde) VALUES ('hocine.djimo@gmail.com', 'HOCINE', 'Djamila', 100000);


