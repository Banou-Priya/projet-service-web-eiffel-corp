CREATE database LocationBDD;

USE LocationBDD; 

CREATE TABLE Employe(
idEmploye VARCHAR(30) PRIMARY KEY,
nomEmploye VARCHAR(30),
prenomEmploye VARCHAR(30),
budgetEmploye FLOAT(10)
);

CREATE TABLE Vehicule(
pk_immatriculation VARCHAR(30) PRIMARY KEY,
modele VARCHAR(30),
marque VARCHAR(30),
couleur VARCHAR(30),
prixLocation FLOAT(10),
prixVente FLOAT(10),
is_dispo BOOL,  
louerAuMoinsUneFois BOOL,
fk_idEmploye VARCHAR(30) REFERENCES Employe (idEmploye) 
);

CREATE TABLE NotesEmploye(
fk_idEmploye VARCHAR(30) REFERENCES Employe(idEmploye),
fk_immatriculation VARCHAR(30) REFERENCES Vehicule(pk_immatriculation),
noteEmploye VARCHAR(50)
);

INSERT INTO Employe (idEmploye, nomEmploye, prenomEmploye, budgetEmploye) VALUES ('100AA123AA', 'Crozat', 'Stéphane', 50000);
INSERT INTO Employe (idEmploye, nomEmploye, prenomEmploye, budgetEmploye) VALUES ('101AA123AA', 'Bernier', 'Emmanuel', 50000);
INSERT INTO Employe (idEmploye, nomEmploye, prenomEmploye, budgetEmploye) VALUES ('102AA123AA', 'Vincent', 'Antoine', 50000);
INSERT INTO Employe (idEmploye, nomEmploye, prenomEmploye, budgetEmploye) VALUES ('103AA123AA', 'Boscolo', 'Corinne', 50000);   

INSERT INTO Vehicule (pk_immatriculation, modele, marque, couleur, prixLocation, prixVente, is_dispo, louerAuMoinsUneFois) VALUES ('AA123AA', 'Clio', 'Renault', 'Noir', 1000, 4000 ,TRUE , FALSE);
INSERT INTO Vehicule (pk_immatriculation, modele, marque, couleur, prixLocation, prixVente, is_dispo, louerAuMoinsUneFois) VALUES ('AB123NB', '807', 'Peugeot', 'Bleu', 1000, 4000, TRUE , FALSE);
INSERT INTO Vehicule (pk_immatriculation, modele, marque, couleur, prixLocation, prixVente, is_dispo, louerAuMoinsUneFois) VALUES ('DE001TR', 'Clio', 'Renault', 'Rouge', 1000, 4000, TRUE , FALSE);
INSERT INTO Vehicule (pk_immatriculation, modele, marque, couleur, prixLocation, prixVente, is_dispo, louerAuMoinsUneFois) VALUES ('AM007JB', '205', 'Peugeot', 'Rose', 1000 , 4000, TRUE , FALSE);
INSERT INTO Vehicule (pk_immatriculation, modele, marque, couleur, prixLocation, prixVente, is_dispo, louerAuMoinsUneFois) VALUES ('BK200OB', 'Cayenne', 'Porsche', 'Noir', 1000, 4000, TRUE, FALSE);
INSERT INTO Vehicule (pk_immatriculation, modele, marque, couleur, prixLocation, prixVente, is_dispo, louerAuMoinsUneFois) VALUES ('ZX987FR', 'Twingo', 'Renault', 'Jaune', 1000 , 4000, TRUE, FALSE);


