CREATE database VenteBDD;

USE VenteBDD; 

CREATE TABLE Client(
adresseMailClient varchar(40) PRIMARY KEY,
nomClient varchar(20),
prenomClient varchar(20)
); 

CREATE TABLE PanierClients(
fk_adresseMailClient varchar(40) REFERENCES Client(adresseMailClient),
fk_immatriculation varchar(30) REFERENCES Vehicule(pk_immatriculation)
); 

INSERT INTO Client (adresseMailClient, nomClient, prenomClient) VALUES ('underworld@gmail.com', 'JERANDO', 'Hamza');
INSERT INTO Client (adresseMailClient, nomClient, prenomClient) VALUES ('under_world@gmail.com', 'JERANDO', 'Hamza');

